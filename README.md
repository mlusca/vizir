## Telzir Test

# Instalação

1. Executar o comando `composer install` na pasta do projeto.
2. Criar um virtual host até a pasta public do projeto, ou acessa-lá diretamente.
   
## Execução do Teste
1. Executar o comando no cmd `./vendor/bin/phpunit tests/MockupTest`
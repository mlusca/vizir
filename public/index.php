<?php
/*
 *  Nessa linha chamo todas as configurações globais 
 *  antes de qualquer chamada, para que ja fique disponível
 *  todas as contantes definidas.
 */
require_once "../config_global.php";

/*
 * Caminho para o autoload do composer, para que carregue as 
 * classes do projeto.
 */
require_once BASE_PATH . "/vendor/autoload.php";

/*
 * O Kernel é o responsável por chamar o sistema, ao acessa-lo.
 */
$kernel = new \App\Kernel();
$kernel->run();
<?php

use PHPUnit\Framework\TestCase;

class MockupTest extends TestCase
{
    public function testRetornaMockup()
    {
        $this->assertIsArray(\App\Model\Plano::all());
        $this->assertIsArray(\App\Model\Tarifa::all());
    }
}
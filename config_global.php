<?php
declare(strict_types=1);
/**
 *  Url para a pasta pública
 */
define('PUBLIC_PATH',  __DIR__."/public");

/**
 *  Caminho para o diretório base
 */
define('BASE_PATH', __DIR__);

/**
 *  Primeiro caminho
 */
define('FIRST_PATH', parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH));

/**
 *  Caminho para as views.
 */
define('VIEW_PATH', __DIR__."/app/View");
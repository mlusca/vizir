<html>

<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
          integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"
            integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
</head>

<body>
<div class="container">
    <form method="POST" action="">
        <div class="form-group">
            <label>Origem</label>
            <select id='tarifa' name='origem' class="form-control">
                <?php
                foreach ($tarifas as $tarifa) {
                    echo "<option> {$tarifa['origem']} </option>";
                }
                ?>
            </select>
        </div>

        <div class="form-group">
            <label>Destino</label>
            <select id="destino" name='destino' class="form-control">
                <option>Selecione primeiro a Origem</option>
            </select>
        </div>

        <div class="form-group">
            <label>Tempo</label>
            <input type="number" value=0 name='tempo' class="form-control"/>
        </div>

        <div class="form-group">
            <label>Plano</label>
            <select id="plano" name='plano' class="form-control">
                <?php
                foreach ($planos as $plano) {
                    echo "<option value='{$plano["number"]}'> {$plano['plano']} </option>";
                }
                ?>
            </select>
        </div>

        <button type="submit" class="btn btn-success">Calcular</button>
    </form>
    <br/>

    <div class="row">
        <?php
        if (isset($valorPlano)) {
            ?>
            <div class="col-6">
                <div class="card">
                    <div class="card-header">
                        <h6>Com o plano :)</h6>
                    </div>
                    <div class="card-body">
                        R$ <?= number_format($valorPlano, 2, ',', '.') ?>
                    </div>
                </div>
            </div>
            <?php
        }


        if (isset($valorSemPlano)) {
            ?>
            <div class="col-6">
                <div class="card">
                    <div class="card-header">
                        <h6>Sem o plano :(</h6>
                    </div>
                    <div class="card-body">
                        R$ <?= number_format($valorSemPlano, 2, ',', '.') ?>
                    </div>
                </div>
            </div>
            <?php
        }
        ?>
    </div>

</div>
<script>
    function recuperarOptions(tarifa) {
        $.get('./tarifa-destino?tarifa=' + tarifa, (e) => {
            let itens = JSON.parse(e);
            $('#destino').find('option')
                         .remove()
                         .end()
                         .append('<option readonly>Selecionar</option>')
                         .val('whatever')
            Object.keys(itens).forEach(key => {
                $("#destino").append(new Option(itens[key]['destino'], itens[key]['destino']));
            })
        })
    }

    $(document).ready(function () {
        recuperarOptions('011');

        $('#tarifa').on('change', function () {
            recuperarOptions($(this).val());
        });
    })
</script>
</body>

</html>
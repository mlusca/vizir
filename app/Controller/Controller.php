<?php

namespace App\Controller;


use App\ViewComposer;

class Controller
{
    public $view;

    public function __construct()
    {
        $this->view = new ViewComposer();
    }

    public function addSlashesArray($input_arr){
        if(is_array($input_arr)){
            $tmp = array();
            foreach ($input_arr as $key1 => $val){
                $tmp[$key1] = $this->addSlashesArray($val);
            }
            return $tmp;
        }else{
            return addslashes($input_arr);
        }
    }
}
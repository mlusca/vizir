<?php

namespace App\Controller;

use App\Model\Plano;
use App\Model\Tarifa;

class VizirController extends Controller
{
    /**
     * Método de entrada da route.
     */
    public function index()
    {
        $tarifas = Tarifa::all();
        $planos = Plano::all();
        return $this->view->compose('index', compact('tarifas', 'planos'));
    }

    /**
     * Apenas retorna uma lista de tarifa correspondente ao get.
     *
     * @return false|string
     */
    public function tarifaDestino()
    {
        $tarifas = Tarifa::all();
        $tarifa = $_GET['tarifa'];

        return json_encode(array_filter($tarifas, function ($key) use ($tarifa) {
            return $key['origem'] === $tarifa;
        }, ARRAY_FILTER_USE_BOTH));
    }

    /**
     * Recebe os requests e chama calcularPlano
     */
    public function preparacaoCalculo()
    {
        $tarifas = Tarifa::all();
        $planos = Plano::all();

        $origem  = $_POST['origem'];
        $destino = $_POST['destino'];
        $plano   = $_POST['plano'];
        $tempo   = $_POST['tempo'];

        $tarifa = array_filter($tarifas, function ($key) use ($origem, $destino) {
            return $key['origem'] === $origem && $key['destino'] === $destino;
        }, ARRAY_FILTER_USE_BOTH);

        $valorPlano = $this->calcularPlano(reset($tarifa), $plano, $tempo);
        $valorSemPlano = $this->calcularSemPlano(reset($tarifa), $tempo);

        return $this->view->compose('index', compact('tarifas', 'planos', 'valorPlano', 'valorSemPlano'));
    }

    /**
     * Retorna o valor com o plano
     */
    private function calcularPlano($tarifa, $plano, $tempo)
    {
        $resto = $tempo - $plano;

        if ($resto > 0) {
            return $resto * $tarifa['preco'] * 1.1;
        }

        return 0;
    }

    /**
     * Retorna o valor sem o plano
     */
    private function calcularSemPlano($tarifa, $tempo)
    {
        if ($tempo > 0) {
            return $tempo * $tarifa['preco'];
        }

        return 0;
    }
}

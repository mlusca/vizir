<?php

namespace App\Model;

/**
 *  Classe Mockup para o teste.
 */
class Plano
{
    private $plano = [
        [
            'plano' => 'FaleMais 30',
            'number' => 30
        ],
        [
            'plano' => 'FaleMais 60',
            'number' => 60
        ],
        [
            'plano' => 'FaleMais 120',
            'number' => 120
        ],
    ];

    public static function all()
    {
        $plano = new Plano();
        return $plano->plano;
    }
}

<?php
namespace App\Model;

/**
 *  Classe Mockup para o teste.
 */
class Tarifa {
    private $tarifas = [
        [
            'origem' => '011',
            'destino' => '016',
            'preco' => '1.90'
        ],
        [
            'origem' => '016',
            'destino' => '011',
            'preco' => '2.90'
        ],
        [
            'origem' => '011',
            'destino' => '017',
            'preco' => '1.70'
        ],
        [
            'origem' => '017',
            'destino' => '011',
            'preco' => '2.70'
        ],
        [
            'origem' => '011',
            'destino' => '018',
            'preco' => '0.90'
        ],
        [
            'origem' => '018',
            'destino' => '011',
            'preco' => '1.90'
        ],
    ];

    public static function all() {
        $tarifa = new Tarifa();
        return $tarifa->tarifas;
    }
}
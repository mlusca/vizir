<?php

\App\Route::get('/', 'VizirController@index');
\App\Route::get('/tarifa-destino', 'VizirController@tarifaDestino');
\App\Route::post('/', 'VizirController@preparacaoCalculo');

<?php

namespace App;

class ViewComposer
{
    public function compose(string $viewName, $data = [])
    {
        $path = VIEW_PATH . "/" . str_replace('.', '/', $viewName) . ".php";

        foreach ($data as $key => $v) {
            $$key = $v;
        }

        file_exists($path) ? include($path) : 'View inexistente.';
    }
}

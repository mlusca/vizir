<?php

namespace App;

class Route extends Kernel
{

    protected static $gets;
    protected static $posts;
    private static $url;
    private $instance;

    /**
     * @param $argument
     */
    static function init()
    {
        $instance = new self;
        $instance->readRoutes();
    }

    private function checkRoute()
    {
        $route = $this->getRoutesFromRequestMethod();
        if (!isset($route[$this->transformRoute()])) {
            echo "Rota inexistente";
            return;
        }

        $this->executeRoute();
    }

    /**
     *  Check a route
     */
    private function readRoutes()
    {
        require_once BASE_PATH . "/app/routes.php";

        $this->checkRoute();
    }

    /**
     * @return string
     */
    public function transformRoute()
    {
        return $_SERVER['DOCUMENT_ROOT'] . $this->route;
    }

    public function getRoutesFromRequestMethod()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'GET') {
            return self::$gets;
        } else {
            return self::$posts;
        }
    }

    /**
     *
     */
    public function executeRoute()
    {
        $parts = explode('@', $this->getRoutesFromRequestMethod()[$this->transformRoute()][0]);

        $class = 'App\\Controller\\' . $parts[0];

        if (!class_exists($class)) {
            echo "Erro, Controller não existe!";
            die();
        }

        if (!method_exists($class, $parts[1])) {
            echo "Erro, método inexiste!";
            die();
        }

        $instance = new $class();
        echo $instance->{$parts[1]}();
    }

    /**
     * @param string $url
     * @param string $controller
     *
     * @return array
     */
    public static function get(string $url, string $controller)
    {
        return self::$gets[str_replace('\\', '/', PUBLIC_PATH . $url)] = [$controller];
    }

    /**
     * @param string $url
     * @param string $controller
     *
     * @return array
     */
    public static function post(string $url, string $controller)
    {
        return self::$posts[str_replace('\\', '/', PUBLIC_PATH . $url)] = [$controller];
    }
}

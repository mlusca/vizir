<?php

namespace App;

/**
 * Primeira classe executada pelo sistema.
 * Esta classe é responsável por direcionar a execução da rota.
 *
 * Esta base de projeto foi desenvolvida por mim para
 * o teste da Vizir.
 */
class Kernel
{

    protected $route;

    /**
     * Kernel constructor.
     */
    public function __construct()
    {
        $this->route = $this->getURLRoute();
    }

    /**
     * Primeiro método executado ao acessar o sistema.
     *
     */
    public function run()
    {
        return Route::init($this->route);
    }

    /**
     * Método responsável por capturar a URL que o usuário está acessando.
     * Todas as rotas que o usuário acessa são redirecionadas para o index.php,
     * com isso capturamos através do .htaccess todos os excedentes e montamos a URL no Routes.
     */
    public function getURLRoute()
    {
        return str_replace(PUBLIC_PATH, '', parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH));
    }
}
